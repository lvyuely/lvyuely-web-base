<!DOCTYPE html>
<html>
	<head>
		<title>Web基础配置-用户登录</title>
		<meta charset="utf-8" />
	</head>
	<body>
	  <h1>用户登录</h1>
	  <form method="post" action="login">
	  		<input type="text" name="username" placeholder="用户名" value="${username?if_exists}" /><br />
	  		<input type="password" name="password" placeholder="密码" value="${password?if_exists}"/><br />
	  		<button>登录</button>
	  </form>
	  ${error?if_exists}
	</body>
</html>