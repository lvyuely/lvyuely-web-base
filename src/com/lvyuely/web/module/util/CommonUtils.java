package com.lvyuely.web.module.util;

import java.util.Set;

import org.apache.commons.lang.StringUtils;

public final class CommonUtils {

	/**
	 * 简单IP校验
	 * 
	 * @param ip
	 * @return
	 */
	public static boolean isLocalIp(String ip) {
		if (StringUtils.isNotEmpty(ip)) {
			Set<String> ips = SystemUtils.getNetInterfaceAddress();
			if (ips == null) {
				return false;
			}
			return ips.contains(ip);
		}
		return false;
	}
}
