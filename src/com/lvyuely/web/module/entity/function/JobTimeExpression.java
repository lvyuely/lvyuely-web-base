package com.lvyuely.web.module.entity.function;

import java.io.Serializable;

public class JobTimeExpression implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	// 定时任务 ID
	private String job;
	// 定时任务名称
	private String name;
	// 运行服务器 IP
	private String serverIp = "";
	// 触发器 ID
	private String trigger;
	// 触发器类型
	private String triggerType;
	// 时间规则
	private String expression;
	// 触发器周期
	private long inteval;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public long getInteval() {
		return inteval;
	}

	public void setInteval(long inteval) {
		this.inteval = inteval;
	}

}
