package com.lvyuely.web.module.service.auth.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lvyuely.web.module.entity.auth.Account;
import com.lvyuely.web.module.mapper.auth.AccountMapper;
import com.lvyuely.web.module.service.auth.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	private final static Logger logger = Logger.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountMapper accountMapper;

	@Override
	public boolean save(Account vo) {
		try {
			return accountMapper.save(vo) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public boolean deleteById(Long id) {
		try {
			return accountMapper.deleteById(id) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public boolean update(Account vo) {
		try {
			return accountMapper.update(vo) > 0;
		} catch (Throwable t) {
			logger.error("", t);
		}
		return false;
	}

	@Override
	public Account getById(Long id) {
		try {
			return accountMapper.getById(id);
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

	@Override
	public List<Account> listAll() {
		try {

			return accountMapper.listAll();
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

	@Override
	public List<Account> listByPage(long start, int length) {
		try {
			return accountMapper.listByPage(start, length);
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

	@Override
	public Account login(Account account) {
		try {
			return accountMapper.login(account);
		} catch (Throwable t) {
			logger.error("", t);
		}
		return null;
	}

}
