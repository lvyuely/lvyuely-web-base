package com.lvyuely.web.module.service.auth;

import com.lvyuely.web.module.entity.auth.Account;
import com.lvyuely.web.module.service.BaseService;

public interface AccountService extends BaseService<Long, Account> {

	public Account login(Account account);

}
