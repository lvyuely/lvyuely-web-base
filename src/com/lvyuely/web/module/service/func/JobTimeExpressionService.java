package com.lvyuely.web.module.service.func;

import java.util.List;

import com.lvyuely.web.module.entity.function.JobTimeExpression;

public interface JobTimeExpressionService {

	public boolean save(JobTimeExpression vo);

	public boolean deleteById(Long id);

	public boolean update(JobTimeExpression vo);

	public JobTimeExpression getById(Long id);

	public List<JobTimeExpression> listAll();

	public List<JobTimeExpression> listByPage(long start, int length);

}
