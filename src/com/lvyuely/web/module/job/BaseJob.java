package com.lvyuely.web.module.job;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.lvyuely.web.module.util.CommonUtils;

public abstract class BaseJob {

	private final static Logger logger = Logger.getLogger(BaseJob.class);

	private String serverIp;

	public abstract void work();

	/**
	 * 运行验证规则
	 * 
	 * @return
	 */
	public boolean runCheck() {
		if (StringUtils.isNotEmpty(serverIp)) {
			logger.info("server ip isn`t empty,now check the ip weather is local server ip");
			String[] temp = serverIp.split(",");
			for (String tmp : temp) {
				if (CommonUtils.isLocalIp(tmp)) {
					return true;
				}
			}
		} else {
			logger.info("server ip is empty,all server sun");
			return true;
		}
		return false;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

}
