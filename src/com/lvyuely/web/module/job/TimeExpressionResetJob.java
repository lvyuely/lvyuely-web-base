package com.lvyuely.web.module.job;

import org.apache.log4j.Logger;

public class TimeExpressionResetJob extends BaseJob {

	private final static Logger logger = Logger.getLogger(TimeExpressionResetJob.class);

	@Override
	public void work() {
		if (!runCheck()) {
			logger.info("local server check  failed, exit!!!");
			return;
		}
		logger.info("我开始执行了");
	}

}
