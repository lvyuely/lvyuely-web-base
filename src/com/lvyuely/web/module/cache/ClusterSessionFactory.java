package com.lvyuely.web.module.cache;

import java.util.Calendar;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.whalin.MemCached.MemCachedClient;

public class ClusterSessionFactory {

	private static final Logger logger = Logger.getLogger(ClusterSessionFactory.class);

	private final int sessionKeepTime = 30 * 60; // 三十分钟

	private final MemCachedClient client;

	public ClusterSessionFactory(MemCachedClient memcachedClient) {
		this.client = memcachedClient;
	}

	public synchronized ClusterSession create() {
		UUID uuid = UUID.randomUUID();
		ClusterSession session = new ClusterSession();
		session.setId(uuid.toString());
		return session;
	}

	/**
	 * 存储 Session
	 * 
	 * @param session
	 * @return
	 */
	public boolean store(ClusterSession session) {
		if (session == null) {
			return false;
		}
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.SECOND, sessionKeepTime);
			if (client.set(session.getId(), session, calendar.getTime())) {
				logger.info("Memcached put:key->[" + session.getId() + "];value->[" + session + "];cacheTime->[" + calendar.getTime() + "] cached success...");
				return true;
			} else {
				logger.error("Memcached put:key->[" + session.getId() + "];value->[" + session + "] cached fail...");
			}
		} catch (Throwable t) {
			logger.error("Memcached put:key->[" + session.getId() + "];value->[" + session + "] cached fail...");
		}
		return false;
	}

	/**
	 * 判断session是否存在
	 * 
	 * @param sessionid
	 * @return
	 */
	public boolean exists(String sessionid) {
		return client.keyExists(sessionid);
	}

	/**
	 * 刷新Session信息
	 * 
	 * @param sessionid
	 * @return
	 */
	public boolean flush(String sessionid) {
		ClusterSession session = (ClusterSession) client.get(sessionid);
		if (session != null && this.store(session)) {
			logger.info("sessionid:" + sessionid + " success flush!");
			return true;
		} else {
			logger.error("sessionid:" + sessionid + " not exists!");
			return false;
		}
	}

	/**
	 * 根据ID 查询Session
	 * 
	 * @param sessionid
	 * @return
	 */
	public ClusterSession get(String sessionid) {
		return (ClusterSession) client.get(sessionid);
	}

	public long incr(String key) {
		return client.incr(key);
	}

}
